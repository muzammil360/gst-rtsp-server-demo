# Introduction
As part of this project we will run an rtsp server (via gstreamer) which will live stream a camera feed. The camera feed will be h265 encoded using hw encoding.


# Installation

```
sudo apt install g++ cmake make pkg-config

sudo apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-pulseaudio

sudo apt-get install libgstrtspserver-1.0-dev gstreamer1.0-rtsp
```

# Build and run
```
# cd <repo folder>
mkdir -p build && cd build
cmake .. && make
./server a
```

# Contact 
For questions, send me email at muzammil360@gmail.com
